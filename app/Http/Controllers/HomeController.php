<?php

namespace App\Http\Controllers;

use App\Trailers;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $trailers = Trailers::all();
        return view('welcome', ['trailers' => $trailers]);
    }
}
