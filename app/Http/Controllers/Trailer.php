<?php

namespace App\Http\Controllers;

use App\LikedUsers;
use App\Trailers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Trailer extends Controller
{
    public function trailerView($id) {
        $trailer = Trailers::findorfail($id);
        if (Auth::user()) {
            $getLike = LikedUsers::where('user', Auth::user()->id)->where('trailer', $id)->first();
        } else {
            $getLike = false;
        }
        $like = $getLike ? 'true' : 'false';
        return view('trailer', ['trailer' => $trailer, 'like' => $like]);
    }

    public function likeTrailer($id) {
        $getLike = LikedUsers::where('user', Auth::user()->id)->where('trailer', $id)->first();
        if ($getLike) {
            LikedUsers::find($getLike->id)->delete();
            return false;
        } else {
            $like = new LikedUsers();
            $like->user = Auth::user()->id;
            $like->trailer = $id;
            $like->save();
            return true;
        }
    }
}
