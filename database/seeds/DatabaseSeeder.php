<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create users
         */
        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@testtask.ru';
        $admin->password = bcrypt('12345678');
        $admin->email_verified_at = now();
        $admin->remember_token = Str::random(10);
        $admin->save();

        factory(User::class, 10)->create();

        /**
         * Import 10 trailers
         */
        $this->call(ImportTrailersSeeder::class);
    }
}
