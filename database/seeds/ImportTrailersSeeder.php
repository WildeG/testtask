<?php

use App\Trailers;
use Illuminate\Database\Seeder;

class ImportTrailersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rss_xml = simplexml_load_file("https://trailers.apple.com/trailers/home/rss/newtrailers.rss");
        $num = 0;
        foreach ($rss_xml->channel->item as $item) {
            if ($num >= 10) continue;
            $trailer = new Trailers();
            $trailer->title = $item->title;
            $trailer->link = $item->link;
            $trailer->trailer = $item->description;
            $trailer->save();
            $num++;
        }
    }
}
