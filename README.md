## Установка

! У вас должен быть установлен [composer](https://getcomposer.org/) и npm ([для Windows](https://nodejs.org/ru/))

- Клонируйте репозиторий
- Перейдите в клонированную папку
- Выполните следующие команды по порядку
    - composer i
    - npm i
    - npm run dev
- Скопируйте файл .env.example и сохраните под именем .env
-- Произведите изменения следующих параметров внутри файла:


    DB_CONNECTION=mysql  
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=laravel
    DB_USERNAME=root
    DB_PASSWORD=


- Выполните следующие команды по порядку
    - php artisan key:generate
    - php artisan migrate --seed
    - php artisan serve
- Откройте в браузере ссылку - [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

Готово.

***

Для авторизации можете зарегистрироваться или использовать тестовые данные:
- **Логин:** admin@testtask.ru
- **Пароль:** 12345678
