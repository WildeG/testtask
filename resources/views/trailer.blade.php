@extends('layouts.app')

@section('script')
    <script type="text/javascript">
        let app = new Vue({
            el: '#app',
            data: {
                like: {{ $like }},
                idTrailer: {{ $trailer->id }}
            },
            methods: {
                clickBtn: function() {
                    let self = this;
                    self.like = !self.like;
                    axios({
                        method: 'post',
                        url: '/trailer/'+self.idTrailer+'/like'
                    }).then(function (response) {
                        self.like = response.data;
                    }).catch(function (response) {
                        alert('Произошла ошибка');
                    });
                }
            },
            computed: {
                btnName() {
                    let self = this;
                    if (self.like) {
                        return 'Вам понравилось'
                    } else {
                        return 'Мне нравится'
                    }
                }
            }
        });
    </script>
@endsection

@section('content')
    <div class="container">
        <a href="/">Вернуться к списку трейлеров</a>
        <h1 class="mb-3">
            {{ $trailer->title }}
            @auth()
                <button class="float-right btn-outline-primary" v-on:click="clickBtn()"> @{{ btnName }} </button>
            @endauth
        </h1>
        <img class="img-fluid mb-3" src="{{ $trailer->link.'/images/background.jpg' }}">
        <p>{{ $trailer->trailer }}</p>
        <a href="{{ $trailer->link }}" class="btn btn-outline-primary">Перейти к трейлеру</a>
    </div>
@endsection
