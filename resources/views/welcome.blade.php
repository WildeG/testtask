@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($trailers as $trailer)
                <div class="col-4 mb-3">
                    <div class="card">
                        <img class="card-img-top" src="{{ $trailer->link.'/images/background.jpg' }}" alt="Card image cap">
                        <div class="card-body">
                            <a href="/trailer/{{ $trailer->id }}"><h5 class="card-title">{{ $trailer->title }}</h5></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
